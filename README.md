**Índice**  

 [[_TOC_]]  
  
  
# Metodología de trabajo entre Diseño e Integración
En general, cuando se tenga que hacer un nuevo panel, o realizar cambios visuales a un panel existente como por ejemplo añadir gráficas nuevas o reemplazar unas gráficas por otras, es decir, cuando llegue un trabajo que requiera esfuerzo del equipo de DISEÑO y esfuerzo del equipo DESARROLLO, entonces se establece la siguiente forma de trabajo:
- El equipo diseño empezará siempre el trabajo, siguiendo [las pautas de trabajo de git definidas en nuestros estándares](https://gitlab.com/vlci-public/estandares-vlci/git/-/blob/main/README.md). Diseño empezará trabajando en LOCAL y haciendo todas las modificaciones estáticas: HTML + CSS. 
- Cuando el equipo de diseño haya creado la maqueta y/o los cambios estéticos, le dará el relevo al equipo de desarrollo proporcionándole la FeatureBranch con todos los cambios allí comiteados (sin mergear a la rama test / sin haber subido a PRE). En ese momento el equipo de diseño se queda parado, sin posibilidad de hacer cambios en esa rama.
- En este punto el equipo de desarrollo realiza toda la parte dinámica (Javascript, CDAs, etc). Y cuando todo está probado en local y funcionando (con el código comiteado en la FeatureBranch) se le pasa el relevo de nuevo al equipo de diseño para que hagan los últimos ajustes estéticos/visuales.
- Una vez ambos equipos están contentos con los cambios en LOCAL (FeatureBranch), se pasan los cambios con un MR a la rama a test y se instala en PRE. La FeatureBranch muere. 
- A partir de ahí ya se arreglan bugs por parte de cada equipo, cada uno con ramas propias creadas a partir de la rama test.
 
